import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { BlockUI, NgBlockUI } from 'ng-block-ui';
import { Observable, Subscription } from 'rxjs';
import { ConnectToStarWarsApiService } from '../services/connect-to-star-wars-api.service';


@Component({
  selector: 'app-character-details',
  templateUrl: './character-details.component.html',
  styleUrls: ['./character-details.component.scss']
})
export class CharacterDetailsComponent {
  @Input() selectedCharacterDetail;
  @BlockUI('data-list') dataListBlockUI: NgBlockUI;
  getStarWarsPeopleListSubscription$: Subscription | undefined;
  peopleList: any = [];
  keyword: string = '';
  characterID: any
  page: number = 1;
  url=""
  constructor(private _starWarsService: ConnectToStarWarsApiService) { }
  ngOnInit(): void {
    this.getPeopleList()
  }
  getPeopleList() {
    let payload = {
      page:this.page,
      search: this.keyword,
      id :this.characterID
    }
    this.dataListBlockUI.start();
    this.getStarWarsPeopleListSubscription$ = this._starWarsService.getPeopleList(payload).subscribe(
      (res) => {
        if (res) {
          this.peopleList = res.results;
          this.dataListBlockUI.stop();
        }
      }, (err) => {
        this.dataListBlockUI.stop();
      }
    )
  }
  backToIndex(){
    window.location.reload();
  }
}
