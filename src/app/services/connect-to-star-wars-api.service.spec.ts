import { TestBed } from '@angular/core/testing';

import { ConnectToStarWarsApiService } from './connect-to-star-wars-api.service';

describe('ConnectToStarWarsApiService', () => {
  let service: ConnectToStarWarsApiService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ConnectToStarWarsApiService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
