import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})

export class ConnectToStarWarsApiService {
  basUrl = "https://swapi.dev/api/";
  constructor(private https : HttpClient) { }
  getPeopleList(payload: any):Observable<any>{
    return this.https.get<any>(this.basUrl+'people/?page=' + payload.page + 
    "&search=" + payload.search+"&id:" +payload.id)
  }
}
