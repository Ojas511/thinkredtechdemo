import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class GlobalOrgService {

    private _listners = new Subject<any>();
    private _clrlistners = new Subject<any>();
    private _themelistners = new Subject<any>();

    listen(): Observable<any> {
        return this._listners.asObservable();
    }
    clrlisten(): Observable<any> {
        return this._clrlistners.asObservable();
    }
    themelisten(): Observable<any> {
        return this._themelistners.asObservable();
    }
    getResult() {
        this._listners.next('');
    }
    clrResult() {
        this._clrlistners.next('');
    }
    themeColor() {
        this._themelistners.next('');
    }
}
