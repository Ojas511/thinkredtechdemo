import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SharedService {

  private dataSourceForMessage = new BehaviorSubject<any>(null);
  dataSourceForMessage$ = this.dataSourceForMessage.asObservable();

  constructor() { }

  sendMessage(data: any) {
    this.dataSourceForMessage.next(data);
  }

  getDecodedObj(key) {
    let data = atob(key);
    return JSON.parse(decodeURIComponent(escape(data)));
  }

  getEncodedObj(obj) {
    return btoa(unescape(encodeURIComponent(JSON.stringify(obj))));
  }
}
