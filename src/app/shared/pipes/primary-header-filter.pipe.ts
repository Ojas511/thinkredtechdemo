import { Pipe, PipeTransform } from '@angular/core';
import { GlobalConstants } from '../global-constants';
import { Router } from '@angular/router';
import { LoginService } from 'src/app/services/login.service';

@Pipe({
  name: 'primaryHeaderFilter'
})
export class PrimaryHeaderFilterPipe implements PipeTransform {
  mapper = [];
  BASE_ROUTES = GlobalConstants.BASE_ROUTES;

  constructor(
    private _loginService: LoginService, public router: Router
  ) { }

  transform(headerMenu: Array<{}>, current_route: String, permissions: Object, role: String, accessRole: String): unknown {
    // if user removes all routes from path in browser url excpept base route then all the subheaders were displyed
    // logic to handle that
    const arr = headerMenu.map((d) => {
      if (Object.keys(d)[0]) {
        return Object.values(d)[0];
      }
    });
    const includesArr = [];
    if (permissions.hasOwnProperty('primary')) {
      if (current_route == '/') {
        const found = this.BASE_ROUTES.find(d => d.key === permissions['primary'][0].key);
        arr.filter((d) => {
          if (String(d).includes(found['url'])) {
            includesArr.push(d);
          }
        });
      }
    }
    current_route = includesArr.length > 0 ? includesArr[0] : current_route;
    // end
    if (role === 'ADMIN' || role === 'USER') {
      if (permissions.hasOwnProperty('primary')) {

        this.mapper = [];
        const filtered = headerMenu;

        permissions['primary'].forEach(element => {
          const found = filtered.find((ele) => ele['key'] === element.key);
          if (found) {
            this.mapper.push(element);
          }
        });

        filtered.map((item) => {
          item['isDisabled'] = false;
          item['isActive'] = false;
          const found = this.mapper.find((ele) => ele['key'] === item['key']);
          if (found ?.key) {
            if (found ?.checked) {
              item['isActive'] = found['checked'];
            } else {
              item['isDisabled'] = !found['checked'];
            }
          } else {
            item['isDisabled'] = true;
            item['isActive'] = false;
          }
        });
        this._loginService.setAccessRoutes([]);
        this._loginService.setAccessRoutes(filtered);
        return filtered;
      }
    } else {
      const filtered = headerMenu.filter((ele) => {
        if (ele['url'].includes(current_route.split('/')[1])) {
         if (ele['key'].startsWith('sa')) {
            return ele;
          }
        }
      });
      this._loginService.setAccessRoutes([]);
      this._loginService.setAccessRoutes(filtered);
      return filtered;
    }
  }
}
