import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { BlockUI, NgBlockUI } from 'ng-block-ui';
import { Observable, Subscription } from 'rxjs';
import { ConnectToStarWarsApiService } from '../services/connect-to-star-wars-api.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { GlobalConstants } from '../shared/global-constants';

@Component({
  selector: 'app-people',
  templateUrl: './people.component.html',
  styleUrls: ['./people.component.scss']
})
export class PeopleComponent implements OnInit {
  @ViewChild('closeOffCanvas') closeOffCanvas;
  @BlockUI('data-list') dataListBlockUI: NgBlockUI;
  
  getStarWarsPeopleListSubscription$: Subscription | undefined;

  peopleList: any = [];
  keyword: string = '';
  page: number = 1;
  pageSizeOption: number[] = [10, 20, 40, 60, 80, 100];
  pageSize: number = this.pageSizeOption[0];
  totalCount: number = 0;
  characterDetails = false;
  selectedCharacterDetail:any={};
  characterForm: FormGroup;
  gender: any = [{
    key: "Male", value:"male",
  },
  {
    key: "Female", value:"female",
  }]
  constructor(private _starWarsService: ConnectToStarWarsApiService,
    private formBuilder: FormBuilder) { 
    
  }
  
  ngOnInit(): void {
    this.setForm();
    this.getPeopleList();
  }

  searchResult() {
    this.page = 1;
    this.getPeopleList();
  }
  searchClear() {
    if (this.keyword.length === 0) {
      this.page = 1;
      this.getPeopleList();
    }
  }
  searchResultOnEnter(event: any) {
    if (event.key === 'Enter') {
      this.page = 1;
      this.getPeopleList();
    }
  }
  getPeopleList() {
    let payload = {
      page:this.page,
      search: this.keyword,    
    }
    this.dataListBlockUI.start();
    this.getStarWarsPeopleListSubscription$ = this._starWarsService.getPeopleList(payload).subscribe(
      (res) => {
        if (res) {
          this.peopleList = res.results;
          if(localStorage.getItem('newCharacterList')!= undefined){
            this.peopleList = [...JSON.parse(localStorage.getItem('newCharacterList')), ...this.peopleList];
          }
          this.totalCount = res.count;
          this.dataListBlockUI.stop();
        }
      }, (err) => {
        this.dataListBlockUI.stop();
      }
    )
  }
  onPageSizeChange(e:any) {
    this.pageSize = +e;
    this.page = 1;
    this.getPeopleList();
  }
  loadNext(page: number) {
    this.page = page;
    this.getPeopleList();
  }
  openCharacterDetails(item) {
    this.characterDetails = true;
    this.selectedCharacterDetail = item;
    console.log(this.selectedCharacterDetail)
  }
  setForm() {
    this.characterForm = this.formBuilder.group({
      name: [null, [Validators.required]],
      height: [null],
      mass: [null],
      hair_color: [''],
      skin_color:[''],
      eye_color: [''],
      birth_year: [''],
      gender: [null, [Validators.required]],
      homeworld: [''],
      films: [[]],
      species:[[]],
      vehicles: [[]],
      starships: [[]],
      created: [new Date()]
    });
  }
  getEmptyMsg(flag) {
    return GlobalConstants.VALIDATION_MSGS[flag];
  }
  onAddNew() {
    
  }
  onSave(){
    this.closeOffCanvas.nativeElement.click();
    if(localStorage.getItem('newCharacterList')!= undefined){
      let newCharacters = JSON.parse(localStorage.getItem('newCharacterList'));
      newCharacters.unshift(this.characterForm.value);
      localStorage.setItem('newCharacterList',JSON.stringify(newCharacters));
    }
    else{
      localStorage.setItem('newCharacterList',JSON.stringify([this.characterForm.value]));
      this.peopleList.unshift(this.characterForm.value);
    }
    this.characterForm.reset();
    this.setForm();
    this.getPeopleList();
  }
}
